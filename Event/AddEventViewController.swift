//
//  AddEventViewController.swift
//  Event
//
//  Created by Zarina Syrymbet on 2/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class AddEventViewController: UIViewController {
    
    @IBOutlet weak var eventDescription: UITextView!
    @IBOutlet weak var eventCategory: UITextView!
    @IBOutlet weak var eventDate: UITextView!
    @IBOutlet weak var eventName: UITextView!
    @IBOutlet weak var eventPlace: UITextView!
    @IBOutlet weak var button: UIButton!

    var events: [Event] = []
    var category: Category?

    private let eventManager = EventManager()
       
    override func viewDidLoad() {
        events = eventManager.getEvents()
        super.viewDidLoad()
        button.backgroundColor = UIColor(red: 1, green: 0.46, blue: 0.50, alpha: 1.0)
        button.layer.shadowColor = UIColor.gray.cgColor
        button.layer.shadowOpacity = 0.25
        eventCategory.textColor = .white
        eventCategory.textAlignment = NSTextAlignment.center;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
       
    @IBAction func showCategory(_ sender: Any) {
        let alertController = UIAlertController(title: "", message: "Choose category", preferredStyle: .actionSheet)
        
        let sportAction = UIAlertAction(title: "Sport", style: .default, handler: hundler(alert:))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
        })

        let musicAction = UIAlertAction(title: "Music", style: .default, handler: hundler(alert:))

        let exhibitionAction = UIAlertAction(title: "Exhibition", style: .default, handler: hundler(alert:))
     
        let entertainmentAction = UIAlertAction(title: "Entertainment", style: .default, handler: hundler(alert:))

        alertController.addAction(sportAction)
        alertController.addAction(musicAction)
        alertController.addAction(exhibitionAction)
        alertController.addAction(entertainmentAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    func hundler(alert: UIAlertAction!) {
        
        switch alert.title {
        case "Sport":
            self.eventCategory.backgroundColor = UIColor(red: 0.65, green: 0.47, blue: 1, alpha: 1.0)
            self.eventCategory.text = self.eventManager.getCategoryByName(name: "Sport")?.name
            self.category = self.eventManager.getCategoryByName(name: "Sport")
        case "Music":
            self.eventCategory.backgroundColor = UIColor(red: 0.37, green: 0.80, blue: 0.58, alpha: 1.0)
            self.eventCategory.text = self.eventManager.getCategoryByName(name: "Music")?.name
            self.category = self.eventManager.getCategoryByName(name: "Music")
        case "Exhibition":
            self.eventCategory.backgroundColor = UIColor(red: 0.47, green: 0.73, blue: 1, alpha: 1.0)
            print(self.eventManager.getCategoryByName(name: "Exhibition")?.name as Any)
            self.eventCategory.text = self.eventManager.getCategoryByName(name: "Exhibition")?.name
            self.category = self.eventManager.getCategoryByName(name: "Exhibition")
        default:
            self.eventCategory.backgroundColor = UIColor(red: 1, green: 0.46, blue: 0.50, alpha: 1.0)
            self.eventCategory.text = self.eventManager.getCategoryByName(name: "Entertainment")?.name
            self.category = self.eventManager.getCategoryByName(name: "Entertainment")
        }
    }
   
    
    @IBAction func saveOnPressed(_ sender: Any) {
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yy"
        
        eventManager.addEvent(name: eventName.text!,
                              eventDescription: eventDescription.text!,
                              date: dateFormatter.date(from: eventDate.text!)!,
                              place: eventPlace.text!,
                              category: category!)
        
        navigationController?.popViewController(animated: true)
    }
}

