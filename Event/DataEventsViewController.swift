//
//  DataEventsViewController.swift
//  Event
//
//  Created by Zarina Syrymbet on 2/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class DataEventsViewController: UIViewController {
    
    private let eventManager = EventManager()

    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventPlace: UILabel!
    @IBOutlet weak var eventDescription: UITextView!
    @IBOutlet weak var eventCategory: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var button: UIButton!
    var cellValue: Event?
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if cellValue != nil {
            dateFormatter.dateFormat = "dd.MM.yy"
            eventName.text = cellValue?.name
            eventPlace.text = cellValue?.place
            eventDate.text = dateFormatter.string(from: (cellValue?.date!)!)
            eventDescription.text = cellValue?.eventDescription
            eventCategory.text = cellValue?.category?.name
            eventDescription.textColor = .white
            eventDate.textAlignment = NSTextAlignment.right;

            button.backgroundColor = UIColor(red: 1, green: 0.46, blue: 0.50, alpha: 1.0)
            button.layer.shadowColor = UIColor.gray.cgColor
            button.layer.shadowOpacity = 0.25
            
            if (cellValue!.isFavourite) {
                button.setTitle("Remove from favourites", for: .normal)
            }else {
                 button.setTitle("Add to favourites", for: .normal)
            }
            
        }
        
        switch cellValue?.category?.name {
            case "Sport":
                eventView.backgroundColor = UIColor(red: 0.65, green: 0.47, blue: 1, alpha: 1.0)
                eventDescription.backgroundColor = UIColor(red: 0.65, green: 0.47, blue: 1, alpha: 1.0)
            case "Music":
                eventView.backgroundColor = UIColor(red: 0.37, green: 0.80, blue: 0.58, alpha: 1.0)
                eventDescription.backgroundColor = UIColor(red: 0.37, green: 0.80, blue: 0.58, alpha: 1.0)
            case "Exhibition":
                eventView.backgroundColor = UIColor(red: 0.47, green: 0.73, blue: 1, alpha: 1.0)
                eventDescription.backgroundColor = UIColor(red: 0.47, green: 0.73, blue: 1, alpha: 1.0)
            case "Entertainment":
                eventView.backgroundColor = UIColor(red: 1, green: 0.46, blue: 0.50, alpha: 1.0)
                eventDescription.backgroundColor = UIColor(red: 1, green: 0.46, blue: 0.50, alpha: 1.0)
            default:
                eventView.backgroundColor = UIColor(red: 0.65, green: 0.47, blue: 1, alpha: 1.0)
                eventDescription.backgroundColor = UIColor(red: 0.65, green: 0.47, blue: 1, alpha: 1.0)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dateFormatter.dateFormat = "dd.MM.yy"
        eventManager.updateCertainEvent(name: cellValue!.name!, newName: eventName.text!, newDate: dateFormatter.date(from: eventDate.text!)!, newEventDescription:  eventDescription.text!, newPlace: eventPlace.text!)
    }
    
    @IBAction func addToFavoutiteOnPressed(_ sender: Any) {
        if (!cellValue!.isFavourite) {
            eventManager.updateEvent(name: cellValue!.name!, newIsFavourite: true)
        }else {
            eventManager.updateEvent(name: cellValue!.name!, newIsFavourite: false)
        }
        navigationController?.popViewController(animated: true)
    }

}
