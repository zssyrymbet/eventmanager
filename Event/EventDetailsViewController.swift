//
//  EventDetailsViewController.swift
//  Event
//
//  Created by Zarina Syrymbet on 2/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class EventDetailsViewController: UIViewController {
    @IBOutlet weak var eventDescription: UITextView!
    @IBOutlet weak var eventCity: UILabel!
    @IBOutlet weak var eventCategory: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventView: UITextView!
    
    @IBAction func AddToFavouritesOnPressed(_ sender: Any) {
    }
}
