//
//  EventManager.swift
//  Event
//
//  Created by Zarina Syrymbet on 2/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//
import UIKit
import CoreData

class EventManager {
    
    func addEvent(name: String, eventDescription: String, date: Date,
                  place: String, category: Category) {
        let event = NSManagedObject(entity: getEventEntity(), insertInto: getManagedContext())
        
        event.setValue(name, forKeyPath: "name")
        event.setValue(eventDescription, forKeyPath: "eventDescription")
        event.setValue(date, forKeyPath: "date")
        event.setValue(place, forKeyPath: "place")
        event.setValue(category, forKey: "category")
        event.setValue(false, forKeyPath: "isFavourite")
        
        do {
            try getManagedContext().save()
        } catch let error as NSError {
            print("\(error.userInfo)")
        }
        print(event)
    }
    
    func addCategory() {
        if getCategories().count == 0 {
            let categories = ["Sport", "Music", "Entertainment", "Exhibition"]
            for (index, name) in categories.enumerated() {
                let category = NSManagedObject(entity: getCategoryEntity(), insertInto: getManagedContext())
                category.setValue(index, forKeyPath: "id")
                category.setValue(name, forKeyPath: "name")
            }
            do {
                try getManagedContext().save()
            } catch let error as NSError {
                print("\(error.userInfo)")
            }
        }
    }
    
    
    func getCategoryByName(name: String) -> Category? {
        do {
            let fetchRequest = getFetchCategoryRequest()
            fetchRequest.predicate = NSPredicate(format: "name = %@", name)
            let category = try getManagedContext().fetch(fetchRequest)
            
            if category.isEmpty {
                return nil
            }
            
            return category[0]
        } catch let error as NSError {
            print("\(error.userInfo)")
        }
        return nil
    }
    
    func getCategories() -> [Category] {
        do {
            return try getManagedContext().fetch(getFetchCategoryRequest())
        } catch let error as NSError {
            print("\(error.userInfo)")
        }

        return []
    }
    
    func getEvents() -> [Event] {
        do {
            return try getManagedContext().fetch(getFetchRequest())
        } catch let error as NSError {
            print("\(error.userInfo)")
        }

        return []
    }
    
    func updateEvent(name: String, newIsFavourite: Bool) {
        do {
            
            let fetchRequest = getFetchRequest()
            fetchRequest.predicate = NSPredicate(format: "name = %@", name)
            let events = try getManagedContext().fetch(fetchRequest)
    
            if events.isEmpty {
                print("Cannot find event with such name = \(name)")
                
                return
            }
           
            let event = events[0]
            event.setValue(newIsFavourite, forKeyPath: "isFavourite")
            
            try getManagedContext().save()
        } catch let error as NSError {
            print("\(error.userInfo)")
        }
    }
    
    func updateCertainEvent(name: String, newName: String, newDate: Date, newEventDescription: String, newPlace: String){
        do {
                
            let fetchRequest = getFetchRequest()
            fetchRequest.predicate = NSPredicate(format: "name = %@", name)
            let events = try getManagedContext().fetch(fetchRequest)
    
            if events.isEmpty {
                print("Cannot find event with such name = \(name)")
                
                return
            }
            
            let event = events[0]
            event.setValue(newName, forKeyPath: "name")
            event.setValue(newDate, forKeyPath: "date")
            event.setValue(newEventDescription, forKeyPath: "eventDescription")
            event.setValue(newPlace, forKeyPath: "place")
            
            try getManagedContext().save()
        } catch let error as NSError {
            print("\(error.userInfo)")
        }
    }
    
    private func getAppDelegate() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }

    private func getManagedContext() -> NSManagedObjectContext {
        return getAppDelegate().persistentContainer.viewContext
    }

    private func getEventEntity() -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: "Event", in: getManagedContext())!
    }
    
    private func getCategoryEntity() -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: "Category", in: getManagedContext())!
    }

    private func getFetchRequest() -> NSFetchRequest<Event> {
        return NSFetchRequest<Event>(entityName: "Event")
    }
    
    private func getFetchCategoryRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }
}
