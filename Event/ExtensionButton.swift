////
////  ExtensionButton.swift
////  Event
////
////  Created by Zarina Syrymbet on 3/11/20.
////  Copyright © 2020 Zarina Syrymbet. All rights reserved.
////
//
//import UIKit
//
//extension UIButton {
//     /// Стиль кнопки
//    @IBInspectable var backgroundColor: UIColor? {
//       get {
//           return UIColor(cgColor: self.layer.backgroundColor!)
//       }
//       set {
//           self.layer.backgroundColor = newValue?.cgColor
//       }
//    }
//    /// Применение стиля по его строковому названию
//    private func setupWithStyleNamed(named: String?){
//        if let styleName = named, style = ButtonStyle(rawValue: styleName) {
//            setupWithStyle(style)
//        }
//    }
//    /// Применение стиля по его идентификатору
//    func setupWithStyle(style: ButtonStyle){
//        backgroundColor = style.backgroundColor
//        tintColor       = style.tintColor
//        borderColor     = style.borderColor
//        borderWidth     = style.borderWidth
//        cornerRadius    = style.cornerRadius
//    }
//}
