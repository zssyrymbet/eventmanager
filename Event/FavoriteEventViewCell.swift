//
//  FavoriteEventViewCell.swift
//  Event
//
//  Created by Zarina Syrymbet on 3/3/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class FavoriteEventViewCell: UITableViewCell {
    
    @IBOutlet weak var favouriteView: UIView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventPlace: UILabel!
    @IBOutlet weak var eventCategory: UILabel!
    @IBOutlet weak var eventDate: UILabel!

    func configureFavouriteCell(event: Event){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"

        eventName.text = event.name
        eventDate.text = dateFormatter.string(from: event.date!)
        eventPlace.text = event.place
        eventCategory.text = event.category?.name
        eventDate.textAlignment = NSTextAlignment.right;


        switch eventCategory.text {
            case "Sport":
                favouriteView.backgroundColor = UIColor(red: 0.65, green: 0.47, blue: 1, alpha: 1.0)
            case "Music":
                favouriteView.backgroundColor = UIColor(red: 0.37, green: 0.80, blue: 0.58, alpha: 1.0)
            case "Exhibition":
                favouriteView.backgroundColor = UIColor(red: 0.47, green: 0.73, blue: 1, alpha: 1.0)
            case "Entertainment":
                favouriteView.backgroundColor = UIColor(red: 1, green: 0.46, blue: 0.50, alpha: 1.0)
            default:
                favouriteView.backgroundColor = UIColor(red: 0.65, green: 0.47, blue: 1, alpha: 1.0)
        }
    }
}
