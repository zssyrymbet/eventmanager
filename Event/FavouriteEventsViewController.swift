//
//  FavouriteEventsViewController.swift
//  Event
//
//  Created by Zarina Syrymbet on 2/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class FavouriteEventsViewController: UITableViewController {
    
    private let eventManager = EventManager()
    var events: [Event] = []

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        events = []
              for event in eventManager.getEvents() {
                 if event.isFavourite {
                     events.append(event)
                 }
              }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavoriteEventViewCell
        cell.configureFavouriteCell(event: events[indexPath.row])
        
         return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "DataViewController") {
            if let viewController = segue.destination as? DataEventsViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = events[index]
            }
        }
    }
    
}
